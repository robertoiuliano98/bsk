package tdd.training.bsk;

public class Frame {
	
	protected int firstThrow, secondThrow, frameBonus;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		// To be implemented
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		
		this.isSpare();
		this.isStrike();
		
		if(firstThrow + secondThrow > 10 || firstThrow + secondThrow < 0)
			throw new BowlingException("Invalid Throw");
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		// To be implemented
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		// To be implemented
		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.frameBonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return this.frameBonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 * @throws BowlingException 
	 */
	public int getScore(Game g, int index) throws BowlingException {
		int score = this.firstThrow + this.secondThrow;
		if(this.isSpare() == true && this.isStrike() == false) {
			score = score + this.getBonus();
		} else if (this.isStrike() == true) {
			//System.out.println("flag1");
			if(index<9) {
				//System.out.println("flag2");
				if(index + 1 == 9) {
					score = score + g.getFrameAt(index+1).getFirstThrow() + g.getFrameAt(index+1).getSecondThrow();
				} else if (index + 1 < 9 && g.frames.get(index+1).isStrike() == true) {
					score = score +  g.getFrameAt(index+1).getFirstThrow() + g.getFrameAt(index+2).getFirstThrow();
				} else if (index + 1 < 9 && g.frames.get(index+1).isStrike() == false) {
					score = score + g.getFrameAt(index+1).getFirstThrow() + g.getFrameAt(index+1).getSecondThrow();
				}
			} else {
				score = score + g.getFirstBonusThrow() + g.getSecondBonusThrow();
				if(g.getFirstBonusThrow() == 10) {
					score = score + g.getSecondBonusThrow();
				}
			}
		} 
		return score;
	}
	
	public int getScore() throws BowlingException {
		int score = this.firstThrow + this.secondThrow;
		if(this.isSpare() == true && this.isStrike() == false) {
			score = score + this.getBonus();
		}
		return score;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		if(this.getFirstThrow() == 10) {
			return true;
		} else {
			return false;
		}
	}
	
	public void print()	{
		System.out.println("Frame details:\n");
		System.out.println("First Throw: " + this.getFirstThrow() + "\n");
		System.out.println("Second Throw: " + this.getSecondThrow() + "\n");
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if(this.getFirstThrow() + this.getSecondThrow() == 10) {
			//this.setBonus(this.getFirstThrow());
			return true;
		} else {
			return false;
		}
		
	}

}
