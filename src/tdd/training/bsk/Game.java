package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	 protected ArrayList<Frame> frames;
	 protected int firstBonusThrow, secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<Frame>(10);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() > 9) {
			throw new BowlingException("You can't add other Frame");
		} else {
			this.frames.add(frame);
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index > 9 || index < 0) {
			throw new BowlingException("Index out of range");
		}
		Frame f = frames.get(index);
		return f;	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		//int partial; 
		for (Frame f : frames) {
			//partial = f.getFirstThrow() + f.getSecondThrow();
			if (f.isSpare() == true && f.isStrike() == false) {
				if(frames.indexOf(f)<9) {
				f.setBonus(frames.get(frames.indexOf(f)+1).getFirstThrow());
				} else {
					this.setFirstBonusThrow(7);
					//f.setBonus(this.getFirstBonusThrow());
					score = score + this.getFirstBonusThrow();
				}
				score = score +f.getScore();
			} else if (f.isStrike()) {
				score = score + f.getScore(this, frames.indexOf(f));
			} else if (f.isSpare() == false && f.isStrike() == false){
				score = score + f.getScore();
			}
			//System.out.println(score+"\n");
		}
		return score;
	}

}
