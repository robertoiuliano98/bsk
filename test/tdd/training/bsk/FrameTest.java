package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	//User story 1
	public void testFrame() throws BowlingException {		
		
		int firstThrow = 4;
		int secondThrow = 5;
		
		try {
			
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow,f.getFirstThrow());
			assertEquals(secondThrow,f.getSecondThrow());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
		
		
		
	}
	
	@Test
	//User story 2
	public void testFrameScore(){	
		
		int firstThrow = 1;
		int secondThrow = 5;
		
		try {
			
			Frame f = new Frame(firstThrow, secondThrow);
			//System.out.println("Frame Score: " + f.getScore());
			assertEquals(firstThrow+secondThrow,f.getScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
		
	}
	
	@Test
	//User story 3
	public void testGame(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				game.addFrame(new Frame(1, 5));
				game.addFrame(new Frame(2, 5));
				game.addFrame(new Frame(1, 1));
				game.addFrame(new Frame(4, 2));
				game.addFrame(new Frame(8, 0));
				game.addFrame(new Frame(2, 3));
				game.addFrame(new Frame(1, 3));
				game.addFrame(new Frame(1, 6));
				game.addFrame(new Frame(2, 0));
				game.addFrame(new Frame(10, 0));
				
				//game.getFrameAt(1).print();
				
				assertEquals(new Frame(2, 5).getFirstThrow(),game.getFrameAt(1).getFirstThrow());
				assertEquals(new Frame(2, 5).getSecondThrow(),game.getFrameAt(1).getSecondThrow());

			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	
	@Test
	//User story 4
	public void testGameScore(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(2, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(1, 1);
				game.addFrame(f3);
				Frame f4 = new Frame(4, 2);
				game.addFrame(f4);
				Frame f5 = new Frame(8, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(2, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(1, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(1, 6);
				game.addFrame(f8);
				Frame f9 = new Frame(2, 0);
				game.addFrame(f9);
				Frame f10 = new Frame(7, 0);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
				
				//System.out.println("Bowling Score: " + game.calculateScore());
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 5
	public void testSpare(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(2, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(1, 1);
				game.addFrame(f3);
				Frame f4 = new Frame(4, 2);
				game.addFrame(f4);
				Frame f5 = new Frame(8, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(2, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(1, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(1, 6);
				game.addFrame(f8);
				Frame f9 = new Frame(2, 8);
				game.addFrame(f9);
				Frame f10 = new Frame(3, 7);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+3+f10.getFirstThrow()+f10.getSecondThrow()+7;
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	
	@Test
	//User story 6
	public void testStrike(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(2, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(10, 0);
				game.addFrame(f3);
				Frame f4 = new Frame(4, 2);
				game.addFrame(f4);
				Frame f5 = new Frame(8, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(2, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(1, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 6);
				game.addFrame(f8);
				Frame f9 = new Frame(6, 4);
				game.addFrame(f9);
				Frame f10 = new Frame(10, 0);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+6+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+6+f9.getFirstThrow()+f9.getSecondThrow()+10+f10.getFirstThrow()+f10.getSecondThrow();
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 7
	public void testStrikeAndSpare(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(2, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(10, 0);
				game.addFrame(f3);
				Frame f4 = new Frame(4, 6);
				game.addFrame(f4);
				Frame f5 = new Frame(8, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(2, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(1, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 6);
				game.addFrame(f8);
				Frame f9 = new Frame(6, 4);
				game.addFrame(f9);
				Frame f10 = new Frame(10, 0);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+10+f4.getFirstThrow()+f4.getSecondThrow()+8+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+6+f9.getFirstThrow()+f9.getSecondThrow()+10+f10.getFirstThrow()+f10.getSecondThrow();
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 8
	public void testMultipleStrike(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(10, 0);
				game.addFrame(f2);
				Frame f3 = new Frame(10, 0);
				game.addFrame(f3);
				Frame f4 = new Frame(4, 3);
				game.addFrame(f4);
				Frame f5 = new Frame(8, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(2, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(1, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 6);
				game.addFrame(f8);
				Frame f9 = new Frame(6, 4);
				game.addFrame(f9);
				Frame f10 = new Frame(10, 0);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+14+f3.getFirstThrow()+f3.getSecondThrow()+7+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+6+f9.getFirstThrow()+f9.getSecondThrow()+10+f10.getFirstThrow()+f10.getSecondThrow();
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 9
	public void testMultipleSpares(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(8, 2);
				game.addFrame(f1);
				Frame f2 = new Frame(5, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(7, 2);
				game.addFrame(f3);
				Frame f4 = new Frame(3, 6);
				game.addFrame(f4);
				Frame f5 = new Frame(4, 4);
				game.addFrame(f5);
				Frame f6 = new Frame(5, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(3, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 5);
				game.addFrame(f8);
				Frame f9 = new Frame(8, 1);
				game.addFrame(f9);
				Frame f10 = new Frame(2, 6);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+5+f2.getFirstThrow()+f2.getSecondThrow()+7+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 10
	public void testSpareAsTheLastFrame(){	
		
		// It initializes an empty bowling game.
		Game game = new Game();

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(8, 2);
				game.addFrame(f1);
				Frame f2 = new Frame(5, 5);
				game.addFrame(f2);
				Frame f3 = new Frame(7, 2);
				game.addFrame(f3);
				Frame f4 = new Frame(3, 6);
				game.addFrame(f4);
				Frame f5 = new Frame(4, 4);
				game.addFrame(f5);
				Frame f6 = new Frame(5, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(3, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 5);
				game.addFrame(f8);
				Frame f9 = new Frame(8, 1);
				game.addFrame(f9);
				Frame f10 = new Frame(2, 8);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+5+f2.getFirstThrow()+f2.getSecondThrow()+7+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow()+7;
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 11
	public void testStrikeAsTheLastFrame() throws BowlingException{	
		
		// It initializes an empty bowling game.
		Game game = new Game();
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(1, 5);
				game.addFrame(f1);
				Frame f2 = new Frame(3, 6);
				game.addFrame(f2);
				Frame f3 = new Frame(7, 2);
				game.addFrame(f3);
				Frame f4 = new Frame(3, 6);
				game.addFrame(f4);
				Frame f5 = new Frame(4, 4);
				game.addFrame(f5);
				Frame f6 = new Frame(5, 3);
				game.addFrame(f6);
				Frame f7 = new Frame(3, 3);
				game.addFrame(f7);
				Frame f8 = new Frame(4, 5);
				game.addFrame(f8);
				Frame f9 = new Frame(8, 1);
				game.addFrame(f9);
				Frame f10 = new Frame(10, 0);
				game.addFrame(f10);
				
				int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow()+9;
				
				//System.out.println("Bowling Score: " + game.calculateScore() + " " + score);
				
				assertEquals(game.calculateScore(),score);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}
	
	@Test
	//User story 12
	public void testBestScore() throws BowlingException{	
		
		// It initializes an empty bowling game.
		Game game = new Game();
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);

			try {
				// It adds 10 frames to this bowling game
				Frame f1 = new Frame(10, 0);
				game.addFrame(f1);
				Frame f2 = new Frame(10, 0);
				game.addFrame(f2);
				Frame f3 = new Frame(10, 0);
				game.addFrame(f3);
				Frame f4 = new Frame(10, 0);
				game.addFrame(f4);
				Frame f5 = new Frame(10, 0);
				game.addFrame(f5);
				Frame f6 = new Frame(10, 0);
				game.addFrame(f6);
				Frame f7 = new Frame(10, 0);
				game.addFrame(f7);
				Frame f8 = new Frame(10, 0);
				game.addFrame(f8);
				Frame f9 = new Frame(10, 0);
				game.addFrame(f9);
				Frame f10 = new Frame(10, 0);
				game.addFrame(f10);
				
				//int score = f1.getFirstThrow()+f1.getSecondThrow()+f2.getFirstThrow()+f2.getSecondThrow()+f3.getFirstThrow()+f3.getSecondThrow()+f4.getFirstThrow()+f4.getSecondThrow()+f5.getFirstThrow()+f5.getSecondThrow()+f6.getFirstThrow()+f6.getSecondThrow()+f7.getFirstThrow()+f7.getSecondThrow()+f8.getFirstThrow()+f8.getSecondThrow()+f9.getFirstThrow()+f9.getSecondThrow()+f10.getFirstThrow()+f10.getSecondThrow();
				
				System.out.println("Bowling Score: " + game.calculateScore());
				
				assertEquals(game.calculateScore(),300);
				
			} catch (BowlingException b) {
				System.err.println(b.getMessage());
			}
		
		
	}

}
